package com.ether.kwhbankz.db.model

class KwhRecord(kwhValue: Float) {
    var id: Int = -1
    var value: Float = 0f

    init {
        value = kwhValue
    }

    constructor(id: Int, value: Float) : this(value) {
        this.id = id
        this.value = value
    }
}