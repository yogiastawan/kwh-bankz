package com.ether.kwhbankz.db.model

class TenantRecord(name: String, status: Boolean) {
    var name: String
    var status: Boolean
    var id: Int = -1

    init {
        this.name = name
        this.status = status
    }

    constructor(id: Int, name: String, status: Boolean) : this(name, status) {
        this.name = name
        this.status = status
        this.id = id
    }
}