package com.ether.kwhbankz.db.model

import java.text.SimpleDateFormat
import java.util.*

class DateRecord() {
    var date: String
    var id: Int = -1
    var isWeekly = false
    var isMonthly = false
    private val dateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())

    init {
        date = dateFormat.format(Date())
    }

    constructor(id: Int, date: String, isWeekly: Boolean, isMonthly: Boolean) : this() {
        this.date = date
        this.id = id
        this.isWeekly = isWeekly
        this.isMonthly = isMonthly
    }
}
