package com.ether.kwhbankz.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.ether.kwhbankz.*
import com.ether.kwhbankz.data.DataKwh
import com.ether.kwhbankz.data.KwhVal
import com.ether.kwhbankz.db.model.DateRecord
import com.ether.kwhbankz.db.model.TenantRecord
import com.ether.kwhbankz.edit.rv.KwhRvAdapter
import com.ether.kwhbankz.main.DateRvAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DataBaseHelper(context: Context, private val length: Int) : SQLiteOpenHelper(
    context, DATABASE_NAME, null,
    DATABASE_VERSION
) {

    companion object {
        const val DATABASE_NAME = "kwh.db"
        const val DATABASE_VERSION = 2
        const val KEY_DATE = "date_recorded"
        const val KEY_DATE_WEEKLY = "date_is_weekly"
        const val KEY_DATE_MONTHLY = "date_is_monthly"
        const val KEY_TENANT_NAME = "tenant_name"
        const val KEY_TENANT_STATUS = "tenant_status"
        const val KEY_KWH_VAL = "kwh_value"
        const val KEY_DATE_LABEL = "date_label"
        const val KEY_KWH_LABEL = "kwh_label"
        const val KEY_TENANT_LABEL = "tenant_label"

        const val KEY_TENANT_ID = "tenant_id"
        const val KEY_KWH_ID = "kwh_id"

        const val TAG = "DBHelper"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        //create date table
        db!!.execSQL("CREATE TABLE $TB_DATE (id INTEGER PRIMARY KEY AUTOINCREMENT, $KEY_DATE CHAR(10) UNIQUE NOT NULL, $KEY_DATE_WEEKLY INTEGER NOT NULL, $KEY_DATE_MONTHLY INTEGER NOT NULL)")
        for (i: Int in 0 until length) {
            //create tenant table
            db.execSQL("CREATE TABLE ${TB_TENANT}_${TB_NAME[i]} (id INTEGER PRIMARY KEY AUTOINCREMENT, $KEY_TENANT_NAME CHAR(80) UNIQUE NOT NULL, $KEY_TENANT_STATUS INT NOT NULL)")
            //create kwh table
            db.execSQL("CREATE TABLE ${TB_KWH}_${TB_NAME[i]} (id INTEGER PRIMARY KEY AUTOINCREMENT, $KEY_KWH_VAL DOUBLE NOT NULL)")
            //create table label
            db.execSQL("CREATE TABLE ${TB_LABEL}_${TB_NAME[i]} ( id INTEGER PRIMARY KEY AUTOINCREMENT, $KEY_DATE_LABEL INTEGER NOT NULL, $KEY_TENANT_LABEL INTEGER NOT NULL, $KEY_KWH_LABEL INTEGER NOT NULL)")
        }
//        Log.d(TAG, "on Create")
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        if (p2 > p1) {
            db?.execSQL("ALTER TABLE $TB_DATE ADD COLUMN $KEY_DATE_WEEKLY INTEGER NOT NULL DEFAULT 0")
            db?.execSQL("ALTER TABLE $TB_DATE ADD COLUMN $KEY_DATE_MONTHLY INTEGER NOT NULL DEFAULT 0")
        }
//        Log.d(TAG, "on Upgrade")
    }

    fun deleteTables() {
        val db = this.writableDatabase
        for (i: Int in 0 until length) {
            db.execSQL("DELETE FROM ${TB_TENANT}_${TB_NAME[i]}")
        }
        db.close()
    }

    fun insertDate(date: DateRecord): Long {
        val db = this.writableDatabase
        val value = ContentValues()
        value.put(KEY_DATE, date.date)
        value.put(KEY_DATE_WEEKLY, if (date.isWeekly) 1 else 0)
        value.put(KEY_DATE_MONTHLY, if (date.isMonthly) 1 else 0)
        val numb = db.insertWithOnConflict(TB_DATE, null, value, SQLiteDatabase.CONFLICT_IGNORE)
        date.id = numb.toInt()
        db.close()
        return numb
    }

    fun getDate(idDate: Int): DateRecord? {
        val db = this.readableDatabase

        val cursor = db.rawQuery("SELECT * FROM $TB_DATE WHERE id=$idDate", null)
        if (cursor.moveToFirst()) {
            val d = cursor.getString(cursor.getColumnIndexOrThrow(KEY_DATE))
            val isWeekly = cursor.getInt(cursor.getColumnIndexOrThrow(KEY_DATE_WEEKLY)) == 1
            val isMonthly = cursor.getInt(cursor.getColumnIndexOrThrow(KEY_DATE_WEEKLY)) == 1
            cursor.close()
            db.close()
            return DateRecord(idDate, d, isWeekly, isMonthly)
        }
        db.close()
        return null
    }

    suspend fun getAllDate(dateRvAdapter: DateRvAdapter): Int {
        val db = this.readableDatabase
        val cursor = db.rawQuery("SELECT * FROM $TB_DATE", null)
        if (cursor.moveToFirst()) {
            do {
                withContext(Dispatchers.Main) {
                    dateRvAdapter.addItem(
                        DateRecord(
                            cursor.getInt(cursor.getColumnIndexOrThrow("id")),
                            cursor.getString(cursor.getColumnIndexOrThrow(KEY_DATE)),
                            cursor.getInt(cursor.getColumnIndexOrThrow(KEY_DATE_WEEKLY)) == 1,
                            cursor.getInt(cursor.getColumnIndexOrThrow(KEY_DATE_MONTHLY)) == 1
                        )
                    )
                }

            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return dateRvAdapter.itemCount
    }

    fun insertTenant(pos: Int, tenant: TenantRecord): Long {
        val db = this.writableDatabase
        val value = ContentValues()
        value.put(KEY_TENANT_NAME, tenant.name)
        value.put(KEY_TENANT_STATUS, tenant.status)
        val numb = db.insertWithOnConflict(
            "${TB_TENANT}_${TB_NAME[pos]}",
            null,
            value,
            SQLiteDatabase.CONFLICT_IGNORE
        )
        db.close()
        return numb
    }

    suspend fun getData(pos: Int, dateId: Int, kwhRvAdapter: KwhRvAdapter): Int {
        val db = this.readableDatabase
        val cursor = db.rawQuery(
            "SELECT ${TB_TENANT}_${TB_NAME[pos]}.id AS $KEY_TENANT_ID, $KEY_TENANT_NAME, $KEY_TENANT_STATUS, ${TB_KWH}_${TB_NAME[pos]}.id AS $KEY_KWH_ID, $KEY_KWH_VAL " +
                    "FROM ${TB_TENANT}_${TB_NAME[pos]} " +
                    "LEFT OUTER  JOIN ${TB_LABEL}_${TB_NAME[pos]} ON ${TB_TENANT}_${TB_NAME[pos]}.id=${TB_LABEL}_${TB_NAME[pos]}.$KEY_TENANT_LABEL AND ${TB_LABEL}_${TB_NAME[pos]}.$KEY_DATE_LABEL=$dateId " +
                    "LEFT OUTER JOIN ${TB_KWH}_${TB_NAME[pos]} ON ${TB_KWH}_${TB_NAME[pos]}.id=${TB_LABEL}_${TB_NAME[pos]}.$KEY_KWH_LABEL",
            null
        )
        if (cursor.moveToFirst()) {
            do {
                val id = cursor.getInt(cursor.getColumnIndexOrThrow(KEY_KWH_ID))
                val kwh = cursor.getDouble(cursor.getColumnIndexOrThrow(KEY_KWH_VAL))
                val kwhVal = KwhVal(
                    cursor.getInt(cursor.getColumnIndexOrThrow(KEY_TENANT_ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(KEY_TENANT_NAME)),
                    if (id == 0) null else id,
                    if (kwh <= 0) null else kwh,
                    cursor.getInt(cursor.getColumnIndexOrThrow(KEY_TENANT_STATUS)) == 1
                )
                withContext(Dispatchers.Main) {
                    kwhRvAdapter.addItem(kwhVal)
                }
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return kwhRvAdapter.itemCount
    }

    fun getDataList(pos: Int, dateId: Int): List<DataKwh> {
        val db = this.readableDatabase
        val cursor = db.rawQuery(
            "SELECT $KEY_TENANT_NAME,  $KEY_KWH_VAL " +
                    "FROM ${TB_TENANT}_${TB_NAME[pos]} " +
                    "LEFT OUTER  JOIN ${TB_LABEL}_${TB_NAME[pos]} ON ${TB_TENANT}_${TB_NAME[pos]}.id=${TB_LABEL}_${TB_NAME[pos]}.$KEY_TENANT_LABEL AND ${TB_LABEL}_${TB_NAME[pos]}.$KEY_DATE_LABEL=$dateId " +
                    "LEFT OUTER JOIN ${TB_KWH}_${TB_NAME[pos]} ON ${TB_KWH}_${TB_NAME[pos]}.id=${TB_LABEL}_${TB_NAME[pos]}.$KEY_KWH_LABEL",
            null
        )
        val list = mutableListOf<DataKwh>()
        if (cursor.moveToFirst()) {
            do {
                val dataKwh = DataKwh(
                    cursor.getString(cursor.getColumnIndexOrThrow(KEY_TENANT_NAME)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(KEY_KWH_VAL)),
                )
                list.add(dataKwh)
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        return list
    }

    fun insertKwhValue(pos: Int, dateId: Int, kwhVal: KwhVal): Long {
        val db = this.readableDatabase
        val value = ContentValues()
        value.put(KEY_KWH_VAL, kwhVal.kwh)
        val numb = db.insertWithOnConflict(
            "${TB_KWH}_${TB_NAME[pos]}",
            null,
            value,
            SQLiteDatabase.CONFLICT_IGNORE
        )
        if (numb <= 0) {
            db.close()
            return -1
        }
        kwhVal.kwhValueId = numb.toInt()
        value.clear()
        value.put(KEY_DATE_LABEL, dateId)
        value.put(KEY_TENANT_LABEL, kwhVal.tenantId)
        value.put(KEY_KWH_LABEL, numb)
        db.insertWithOnConflict(
            "${TB_LABEL}_${TB_NAME[pos]}",
            null,
            value,
            SQLiteDatabase.CONFLICT_IGNORE
        )
        db.close()
        return numb
    }

    fun deleteTenant(pos: Int, id: Int): Int {
        val db = this.writableDatabase
        val numb = db.delete(
            "${TB_TENANT}_${TB_NAME[pos]}",
            "id=?",
            arrayOf(id.toString())
        )
        if (numb <= 0) {
            db.close()
            return -1
        }
        val cursor = db.rawQuery(
            "SELECT $KEY_KWH_LABEL FROM ${TB_LABEL}_${TB_NAME[pos]} WHERE $KEY_TENANT_LABEL=$id",
            null
        )
        val kwhIds = mutableListOf<String>()
        if (cursor.moveToFirst()) {
            do {
                kwhIds.add(cursor.getString(cursor.getColumnIndexOrThrow(KEY_KWH_LABEL)))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.delete("${TB_KWH}_${TB_NAME[pos]}", "id IN (${kwhIds.joinToString(",")})", null)
//        Log.d(TAG, "Kwh ids: ${kwhIds.joinToString(",")}")
        kwhIds.clear()
        db.delete(
            "${TB_LABEL}_${TB_NAME[pos]}",
            "$KEY_TENANT_LABEL=?",
            arrayOf(id.toString())
        )
        db.close()
        return numb
    }

    fun deleteKwh(pos: Int, kwhId: Int): Int {
        val db = this.writableDatabase
        val numb = db.delete(
            "${TB_KWH}_${TB_NAME[pos]}",
            "id=?",
            arrayOf(kwhId.toString())
        )
        if (numb <= 0) {
            db.close()
            return -1
        }
        db.delete(
            "${TB_LABEL}_${TB_NAME[pos]}",
            "$KEY_KWH_LABEL=?",
            arrayOf(kwhId.toString())
        )
        db.close()
        return numb
    }

    fun updateKwhValue(pos: Int, kwhId: Int, newValue: Double): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_KWH_VAL, newValue)
        val numb = db.update(
            "${TB_KWH}_${TB_NAME[pos]}",
            values, "id=?", arrayOf(kwhId.toString())
        )
        db.close()
        return numb
    }

    fun updateTenantState(pos: Int, tenantId: Int, isActive: Boolean): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_TENANT_STATUS, if (isActive) 1 else 0)
        val numb =
            db.update("${TB_TENANT}_${TB_NAME[pos]}", values, "id=?", arrayOf(tenantId.toString()))
        db.close()
        return numb
    }

    fun updateTenantName(pos: Int, tenantId: Int, newName: String): Int {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_TENANT_NAME, newName)
        val numb =
            db.update("${TB_TENANT}_${TB_NAME[pos]}", values, "id=?", arrayOf(tenantId.toString()))
        db.close()
        return numb
    }
}