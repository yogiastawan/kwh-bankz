package com.ether.kwhbankz.viewmodel

import androidx.lifecycle.ViewModel
import com.ether.kwhbankz.edit.rv.KwhRvAdapter
import com.ether.kwhbankz.main.DateRvAdapter

class DataShared : ViewModel() {

    val adapter = listOf(
        KwhRvAdapter(), KwhRvAdapter(), KwhRvAdapter(),
        KwhRvAdapter(), KwhRvAdapter(), KwhRvAdapter(), KwhRvAdapter()
    )
    val dateRvAdapter = DateRvAdapter()
}