package com.ether.kwhbankz.edit

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.ether.kwhbankz.R
import com.ether.kwhbankz.data.KwhVal
import com.ether.kwhbankz.db.DataBaseHelper
import com.ether.kwhbankz.db.model.TenantRecord
import com.ether.kwhbankz.edit.dialog.AddTenantDialog
import com.ether.kwhbankz.edit.dialog.SaveFileLoadingDialog
import com.ether.kwhbankz.file.CsvFile
import com.ether.kwhbankz.file.WriteFileContracts
import com.ether.kwhbankz.viewmodel.DataShared
import com.google.android.material.behavior.HideBottomViewOnScrollBehavior
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.*

class EditActivity : AppCompatActivity() {

    private val reqWriteFile =
        registerForActivityResult(WriteFileContracts("text/comma-separated-values")) {
            CoroutineScope(Dispatchers.IO).launch {
                if (it != null) {
                    val dialogSave = SaveFileLoadingDialog.newInstance("CSV")
                    withContext(Dispatchers.Main) {
                        dialogSave.show(supportFragmentManager, SaveFileLoadingDialog.TAG)
                    }
                    val csv = CsvFile(applicationContext)
                    intent.getStringExtra("DATE_STR")?.let { dateStr ->
                        csv.writeFileCSV(
                            it,
                            intent.getIntExtra("DATE_ID", 0),
                            dateStr,
                            dialogSave
                        )
                    }
                }
            }
        }

    private val viewPager2PageChangeCallback = object : ViewPager2.OnPageChangeCallback() {

        override fun onPageScrollStateChanged(state: Int) {
            super.onPageScrollStateChanged(state)
            when (state) {
                ViewPager2.SCROLL_STATE_SETTLING -> {
                    val lp = fabAddTenant.layoutParams
                    if (lp is CoordinatorLayout.LayoutParams) {
                        val behavior = lp.behavior
                        if (behavior is HideBottomViewOnScrollBehavior) {
                            if (behavior.isScrolledDown) {
                                behavior.slideUp(fabAddTenant)
                            } else {
                                fabAddTenant.hide()
                            }
                        }
                    }
                }
                ViewPager2.SCROLL_STATE_IDLE -> {
                    fabAddTenant.show()
                }
                else -> {
                    val lp = fabAddTenant.layoutParams
                    if (lp is CoordinatorLayout.LayoutParams) {
                        val behavior = lp.behavior
                        if (behavior is HideBottomViewOnScrollBehavior) {
                            if (!behavior.isScrolledDown) {
                                fabAddTenant.hide()
                            }
                        }
                    }
                }
            }
        }
    }

    private lateinit var sharedModel: DataShared
    private lateinit var db: DataBaseHelper

    private lateinit var fabAddTenant: FloatingActionButton
    private lateinit var viewPager2: ViewPager2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit)
        setSupportActionBar(findViewById(R.id.toolbar_edit_activity))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_24)
        supportActionBar?.title = intent.getStringExtra("DATE_STR")
        supportActionBar?.elevation = 0f
        val weekly=intent.getBooleanExtra("DATE_WEEKLY",false)
        if (weekly){
            supportActionBar?.subtitle=getString(R.string.weekly)
        }
        val monthly=intent.getBooleanExtra("DATE_MONTHLY",false)
        if (monthly){
            supportActionBar?.subtitle=getString(R.string.monthly)
        }
        db = DataBaseHelper(applicationContext, resources.getStringArray(R.array.level_group).size)
        sharedModel = ViewModelProvider(this).get(DataShared::class.java)
        viewPager2 = findViewById(R.id.viewpager)
        val list = resources.getStringArray(R.array.level_group)
        val listIcon = resources.obtainTypedArray(R.array.tab_icon)
        val idDate = intent.getIntExtra("DATE_ID", 0)
        viewPager2.adapter = CollectionFragmentAdapter(this, list, idDate)
        val tabLayout: TabLayout = findViewById(R.id.tab_layout)
        val tabLayoutMediator = TabLayoutMediator(tabLayout, viewPager2, false, true) { tab, pos ->
            tab.text = list[pos]
            tab.icon = listIcon.getDrawable(pos)
        }
        tabLayoutMediator.attach()
        listIcon.recycle()

        fabAddTenant = findViewById(R.id.fab_add_new_tenant)
        fabAddTenant.setOnClickListener {
            val addTenantDialog = AddTenantDialog.newInstance()
            addTenantDialog.setOnOkCLicked { name, enable ->
                CoroutineScope(Dispatchers.IO).launch {
                    val idTenant = async {
                        db.insertTenant(
                            viewPager2.currentItem,
                            TenantRecord(name, enable)
                        )
                    }
                    val id = idTenant.await()
                    if (id <= 0) {
                        withContext(Dispatchers.Main) {
                            Toast.makeText(
                                applicationContext,
                                getString(R.string.failed_add_tenant, name),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            sharedModel.adapter[viewPager2.currentItem].addItem(
                                KwhVal(
                                    id.toInt(),
                                    name,
                                    null,
                                    null,
                                    enable
                                )
                            )
                            Toast.makeText(
                                applicationContext,
                                getString(R.string.success_add_tenant, name),
                                Toast.LENGTH_SHORT
                            ).show()
                            addTenantDialog.dismiss()
                        }

                    }
                }
            }
            addTenantDialog.show(supportFragmentManager, AddTenantDialog.TAG)
        }

        viewPager2.registerOnPageChangeCallback(viewPager2PageChangeCallback)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewPager2.unregisterOnPageChangeCallback(viewPager2PageChangeCallback)
    }

    override fun onSupportNavigateUp(): Boolean {
        this.onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_bar_menu_edit_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_item_save_to_csv -> {
                saveFileCSV()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun saveFileCSV() {
        CoroutineScope(Dispatchers.IO).launch {
            val date = db.getDate(intent.getIntExtra("DATE_ID", 0))
            withContext(Dispatchers.Main) {
                reqWriteFile.launch(
                    "${date?.date ?: "ETHER"}.csv"
                )
            }
        }

    }
}