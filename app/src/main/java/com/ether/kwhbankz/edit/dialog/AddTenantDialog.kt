package com.ether.kwhbankz.edit.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.ether.kwhbankz.R
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class AddTenantDialog : DialogFragment() {

    var onOkClicked: ((name: String, isEnable: Boolean) -> Unit)? = null

    companion object {
        const val TAG = "ADD_TENANT_DIALOG"
        fun newInstance(): AddTenantDialog {
            return AddTenantDialog()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val v = View.inflate(requireContext(), R.layout.add_tenant_dialog, null)
        val tiLayout: TextInputLayout = v.findViewById(R.id.text_input_layout_add_tenant)
        val tiEditText: TextInputEditText = v.findViewById(R.id.edit_add_name)
        val checkBoxActive: MaterialCheckBox = v.findViewById(R.id.checkbox_active)
        val builder =
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_KwhBankZ_Dialog).apply {
                setTitle(getString(R.string.add_new_tenant_title))
                setView(v)
                setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                setPositiveButton(getString(R.string.add)) { _, _ ->
                    onOkClicked?.invoke(tiEditText.text.toString(), checkBoxActive.isChecked)
                }
            }
        val dialog = builder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
            if (tiEditText.text!!.isBlank()) {
                tiLayout.error = getString(R.string.tenant_blank)
            }
        }
        tiEditText.requestFocus()
        lifecycleScope.launch {
            delay(200)
            val imm =
                tiEditText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(tiEditText, InputMethodManager.SHOW_IMPLICIT)
        }
        tiEditText.doOnTextChanged { text, _, _, _ ->
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = text!!.isNotBlank()
            when {
                text.isBlank() -> {
                    tiLayout.error = getString(R.string.tenant_blank)
                }
                else -> {
                    tiLayout.error = null
                }
            }
        }
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    fun setOnOkCLicked(l: (String, Boolean) -> Unit) {
        onOkClicked = l
    }
}