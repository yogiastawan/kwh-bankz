package com.ether.kwhbankz.edit.rv

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ether.kwhbankz.R
import com.ether.kwhbankz.data.KwhVal

class KwhRvAdapter : RecyclerView.Adapter<KwhRvHolder>() {

    private val list = mutableListOf<KwhVal>()

    private var onItemClicked: ((Int) -> Unit)? = null
    private var onItemLongClicked: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KwhRvHolder {
        val v: View =
            LayoutInflater.from(parent.context).inflate(R.layout.rv_kwh_item, parent, false)
        return KwhRvHolder(v, {
            onItemClicked?.invoke(it)
        }, {
            onItemLongClicked?.invoke(it)
        })
    }

    override fun onBindViewHolder(holder: KwhRvHolder, position: Int) {
        holder.bindToView(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setOnItemClicked(l: ((Int) -> Unit)?) {
        onItemClicked = l
    }

    fun setOnItemLongClicked(l: ((Int) -> Unit)?) {
        onItemLongClicked = l
    }

    fun addItem(kwhVal: KwhVal) {
        list.add(kwhVal)
        notifyItemInserted(itemCount - 1)
    }

    fun removeItem(pos: Int) {
        list.removeAt(pos)
        notifyItemRemoved(pos)
        notifyItemRangeChanged(pos, (itemCount - 1) - pos)
    }

    fun clear() {
        val size = itemCount
        list.clear()
        notifyItemRangeRemoved(0, size)
    }

    fun getKwhVal(pos: Int): KwhVal {
        return list[pos]
    }

    fun toggleState(pos: Int) {
        list[pos].isActive = !list[pos].isActive
        notifyItemChanged(pos)
    }

    fun changeTenantName(pos: Int, newName: String) {
        list[pos].name = newName
        notifyItemChanged(pos)
    }

    fun updateKwh(pos: Int, kwh: Double) {
        list[pos].kwh = kwh
        notifyItemChanged(pos)
    }

    fun clearKwhVal(pos: Int) {
        list[pos].kwh = null
        list[pos].kwhValueId = null
        notifyItemChanged(pos)
    }
}