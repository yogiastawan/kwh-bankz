package com.ether.kwhbankz.edit.dialog

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import com.ether.kwhbankz.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.progressindicator.CircularProgressIndicator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SaveFileLoadingDialog : DialogFragment() {

    private var onCancelClick: (() -> Unit)? = null

    companion object {
        const val TAG = "LOADING_DIALOG"
        fun newInstance(fileName: String): SaveFileLoadingDialog {
            val args = Bundle()
            args.putString("FILE_NAME", fileName)
            val fragment = SaveFileLoadingDialog()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val v = View.inflate(requireContext(), R.layout.save_loading_dialog, null)
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1) {
            CoroutineScope(Dispatchers.Main).launch {
                val progress: CircularProgressIndicator = v.findViewById(R.id.prg)
                progress.visibility = View.GONE
                delay(100)
                progress.visibility = View.VISIBLE
                delay(500)
            }
        }
        val builder =
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_KwhBankZ_Dialog).apply {
                setTitle(getString(R.string.save_file))
                setView(v)
                setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                    onCancelClick?.invoke()
                    dialog.dismiss()
                }
            }

        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    fun setOnCancelClicked(l: () -> Unit) {
        onCancelClick = l
    }
}