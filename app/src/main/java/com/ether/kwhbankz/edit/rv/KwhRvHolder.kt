package com.ether.kwhbankz.edit.rv

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.os.ConfigurationCompat
import androidx.recyclerview.widget.RecyclerView
import com.ether.kwhbankz.R
import com.ether.kwhbankz.data.KwhVal
import com.google.android.material.card.MaterialCardView
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class KwhRvHolder(itemView: View, onItemClicked: (Int) -> Unit, onLongItemCLicked: (Int) -> Unit) :
    RecyclerView.ViewHolder(itemView) {
    init {
        itemView.setOnClickListener {
            onItemClicked(adapterPosition)
        }
        itemView.setOnLongClickListener {
            onLongItemCLicked(adapterPosition)
            true
        }
    }

    fun bindToView(kwhVal: KwhVal) {
        val tenantName: AppCompatTextView = itemView.findViewById(R.id.kwh_name)
        tenantName.text = kwhVal.name
        val card: MaterialCardView = itemView.findViewById(R.id.item_card_view)
        val kwhValue: AppCompatTextView = itemView.findViewById(R.id.kwh_value)
        kwhVal.kwh?.let {

            val formatter = DecimalFormat(
                "#,##0.00",
                DecimalFormatSymbols(
                    ConfigurationCompat.getLocales(itemView.resources.configuration).get(0)
                )
            )
            kwhValue.text = formatter.format(BigDecimal(it))
            if (kwhVal.isActive) {
                card.strokeColor = ContextCompat.getColor(itemView.context, R.color.good_color)
            }
        }

        if (kwhVal.kwh == null) {
            kwhValue.text = ""
            card.strokeColor = ContextCompat.getColor(itemView.context, R.color.error_color)
        }

        if (!kwhVal.isActive) {
            card.strokeColor = ContextCompat.getColor(itemView.context, R.color.default_color)
        }
    }
}