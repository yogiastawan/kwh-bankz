package com.ether.kwhbankz.edit.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.ether.kwhbankz.R
import com.ether.kwhbankz.viewmodel.DataShared
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class EditTenantDialog : DialogFragment() {

    private var onOkClicked: ((String) -> Unit)? = null
    private lateinit var sharedModel: DataShared

    companion object {
        const val TAG = "EDIT_TENANT_DIALOG_TAG"
        fun newInstance(pos: Int, adapterId: Int): EditTenantDialog {
            val args = Bundle()
            args.putInt("ADAPTER_ID", adapterId)
            args.putInt("POS", pos)
            val fragment = EditTenantDialog()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedModel = ViewModelProvider(requireActivity()).get(DataShared::class.java)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val v = View.inflate(requireContext(), R.layout.edit_tenant_dialog, null)
        val pos = requireArguments().getInt("POS")
        val id = requireArguments().getInt("ADAPTER_ID")
        val kwhVal = sharedModel.adapter[pos].getKwhVal(id)
        val tvName: AppCompatTextView = v.findViewById(R.id.name_tenant)
        tvName.text = kwhVal.name
        val tiLayout: TextInputLayout = v.findViewById(R.id.text_input_layout_tenant)
        val tiEditText: TextInputEditText = v.findViewById(R.id.edit_name)
        tiEditText.setText(kwhVal.name)
        val builder =
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_KwhBankZ_Dialog).apply {
                setTitle(getString(R.string.edit_tenant_name))
                setView(v)
                setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                setPositiveButton(getString(R.string.change)) { _, _ ->
                    onOkClicked?.invoke(tiEditText.text.toString())
                }
            }

        val dialog = builder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
            if (tiEditText.text!!.isBlank()) {
                tiLayout.error = getString(R.string.tenant_blank)
            }
        }
        tiEditText.requestFocus()
        lifecycleScope.launch {
            delay(200)
            val imm =
                tiEditText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(tiEditText, InputMethodManager.SHOW_IMPLICIT)
        }
        tiEditText.doOnTextChanged { text, _, _, _ ->
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = text!!.isNotBlank()
            when {
                text.isBlank() -> {
                    tiLayout.error = getString(R.string.tenant_blank)
                }
                text.toString() == kwhVal.name -> {
                    tiLayout.error = getString(R.string.tenant_same_old)
                }
                else -> {
                    tiLayout.error = null
                }
            }
        }
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    fun setOnOkClicked(l: (String) -> Unit) {
        onOkClicked = l
    }
}