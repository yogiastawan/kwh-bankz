package com.ether.kwhbankz.edit

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class CollectionFragmentAdapter(fragmentActivity: FragmentActivity, private val list: Array<String>, private  val idDate:Int) :
    FragmentStateAdapter(fragmentActivity) {
    override fun getItemCount(): Int {
        return list.size
    }

    override fun createFragment(position: Int): Fragment {
        return InputKwhFragment.newInstance(position, idDate)
    }
}