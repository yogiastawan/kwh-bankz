package com.ether.kwhbankz.edit

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.PopupMenu
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ether.kwhbankz.R
import com.ether.kwhbankz.db.DataBaseHelper
import com.ether.kwhbankz.edit.dialog.EditKwhDialog
import com.ether.kwhbankz.edit.dialog.EditTenantDialog
import com.ether.kwhbankz.edit.dialog.InputKwhDialog
import com.ether.kwhbankz.viewmodel.DataShared
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.progressindicator.CircularProgressIndicator
import kotlinx.coroutines.*
import java.lang.reflect.Method

class InputKwhFragment : Fragment() {

    private lateinit var rv: RecyclerView
    private lateinit var tvEmptyAdapter: AppCompatTextView
    private lateinit var circularProgLoadingKwh: CircularProgressIndicator
    private lateinit var sharedModel: DataShared
    private lateinit var db: DataBaseHelper

    companion object {
        fun newInstance(posID: Int, idDate: Int): InputKwhFragment {
            val args = Bundle()
            args.putInt("KEY_ID", posID)
            args.putInt("DATE_ID", idDate)
            val fragment = InputKwhFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedModel = ViewModelProvider(requireActivity()).get(DataShared::class.java)
        db = DataBaseHelper(
            requireContext(),
            requireContext().resources.getStringArray(R.array.level_group).size
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.input_kwh_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvEmptyAdapter = view.findViewById(R.id.tv_empty_adapter)
        circularProgLoadingKwh = view.findViewById(R.id.circular_progress_loading_kwh_data)
        rv = view.findViewById(R.id.rv_kwh_input)
        rv.layoutManager = LinearLayoutManager(requireContext())
        val divider = DividerItemDecoration(
            rv.context,
            LinearLayoutManager.VERTICAL
        )
        divider.setDrawable(
            AppCompatResources.getDrawable(
                requireContext(),
                R.drawable.recyclerview_item_divider
            )!!
        )
        rv.addItemDecoration(divider)
        val id = requireArguments().getInt("KEY_ID")
        rv.adapter = sharedModel.adapter[id]
        sharedModel.adapter[id].setOnItemClicked {
            val kwhVal = sharedModel.adapter[id].getKwhVal(it)
            if (kwhVal.kwhValueId == null || kwhVal.kwhValueId!! <= 0) {
                insertKwhValue(it)
            }
        }
        sharedModel.adapter[id].setOnItemLongClicked {
            val popupMenu = PopupMenu(
                requireContext(),
                rv.findViewHolderForAdapterPosition(it)!!.itemView,
                GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK
            )
            popupMenu.menuInflater.inflate(R.menu.popup_menu, popupMenu.menu)
//            popupMenu.inflate(R.menu.popup_menu)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                popupMenu.setForceShowIcon(true)
            } else {
                try {
                    val fields = popupMenu.javaClass.declaredFields
                    for (field in fields) {
                        if ("mPopup" == field.name) {
                            field.isAccessible = true
                            val menuPopupHelper = field[popupMenu]
                            val classPopupHelper =
                                Class.forName(menuPopupHelper.javaClass.name)
                            val setForceIcons: Method = classPopupHelper.getMethod(
                                "setForceShowIcon",
                                Boolean::class.javaPrimitiveType
                            )
                            setForceIcons.invoke(menuPopupHelper, true)
                            break
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            val kwhVal = sharedModel.adapter[id].getKwhVal(it)
            if (kwhVal.kwhValueId == null) {
                popupMenu.menu.findItem(R.id.menu_item_update_data).title =
                    getString(R.string.insert_kwh_value)
                popupMenu.menu.findItem(R.id.menu_item_clear_data).isEnabled = false
            }
            if (kwhVal.isActive) {
                popupMenu.menu.findItem(R.id.menu_item_enable_tenant).title =
                    getString(R.string.disable_tenant)
            } else if (!kwhVal.isActive) {
                popupMenu.menu.findItem(R.id.menu_item_clear_data).isEnabled = false
            }
            popupMenu.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.menu_item_update_data -> {
                        if (kwhVal.kwhValueId == null) {
                            insertKwhValue(it)
                            true
                        } else {
                            updateKwhValue(it)
                            true
                        }
                    }
                    R.id.menu_item_clear_data -> {
                        clearKwhValue(it)
                        true
                    }
                    R.id.menu_item_enable_tenant -> {
                        //enable disable tenant
                        updateTenantState(it)
                        true
                    }
                    R.id.menu_item_edit_tenant -> {
                        //edit name tenant
                        updateTenantName(it)
                        true
                    }
                    R.id.menu_item_delete_tenant -> {
                        //delete tenant
                        deleteTenant(it)
                        true
                    }
                    else -> {
                        popupMenu.dismiss()
                        false
                    }
                }
            }
            popupMenu.show()
        }
    }

    override fun onResume() {
        super.onResume()
//        load data to adapter
        CoroutineScope(Dispatchers.IO).launch {
            val pos = requireArguments().getInt("KEY_ID")
            if (sharedModel.adapter[pos].itemCount <= 0) {
                withContext(Dispatchers.Main) { circularProgLoadingKwh.visibility = View.VISIBLE }
                val db = DataBaseHelper(
                    requireContext(),
                    requireContext().resources.getStringArray(R.array.level_group).size
                )
                val deferred = async {
                    db.getData(
                        pos,
                        requireArguments().getInt("DATE_ID"),
                        sharedModel.adapter[pos]
                    )
                }
                if (deferred.await() <= 0) {
                    withContext(Dispatchers.Main) {
                        tvEmptyAdapter.visibility = View.VISIBLE
                        rv.visibility = View.GONE
                    }
                }
                withContext(Dispatchers.Main) { circularProgLoadingKwh.visibility = View.GONE }
            }
            withContext(Dispatchers.Main) {
                for (i: Int in 0 until pos - 1) {
                    if (i >= 0) {
                        sharedModel.adapter[i].clear()
                    }
                }
                val max = requireContext().resources.getStringArray(R.array.level_group).size
                for (i: Int in pos + 2 until max)
                    if (i < max) {
                        sharedModel.adapter[i].clear()
                    }
            }
        }
    }

    private fun insertKwhValue(idAdapter: Int) {
        //insert to db
        val id = requireArguments().getInt("KEY_ID")
        val kwhVal = sharedModel.adapter[id].getKwhVal(idAdapter)
        if (kwhVal.isActive) {
            val inputKwhDialog = InputKwhDialog.newInstance(id, idAdapter)
            inputKwhDialog.show(parentFragmentManager, InputKwhDialog.TAG)
            inputKwhDialog.setOnOKClicked { txt ->
                CoroutineScope(Dispatchers.IO).launch {
                    val dateId = requireArguments().getInt("DATE_ID")
                    kwhVal.kwh = txt.toDouble()
                    val idKwh = db.insertKwhValue(id, dateId, kwhVal)
                    withContext(Dispatchers.Main) {
                        if (idKwh > 0) {
                            Toast.makeText(
                                requireContext(),
                                getString(R.string.success_input_kwh, kwhVal.name),
                                Toast.LENGTH_SHORT
                            ).show()
                            sharedModel.adapter[id].notifyItemChanged(idAdapter)
                            inputKwhDialog.dismiss()
                        } else {
                            Toast.makeText(
                                requireContext(),
                                getString(R.string.failed_input_kwh, kwhVal.name),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
        }

    }

    private fun updateKwhValue(idAdapter: Int) {
        val id = requireArguments().getInt("KEY_ID")
        val kwhVal = sharedModel.adapter[id].getKwhVal(idAdapter)

        val editKwhDialog = EditKwhDialog.newInstance(id, idAdapter)
        editKwhDialog.show(parentFragmentManager, EditKwhDialog.TAG)
        editKwhDialog.setOnOkClicked {
            CoroutineScope(Dispatchers.IO).launch {
                val update = async { db.updateKwhValue(id, kwhVal.kwhValueId!!, it.toDouble()) }
                if (update.await() > 0) {
                    withContext(Dispatchers.Main) {
                        sharedModel.adapter[id].updateKwh(idAdapter, it.toDouble())
                        Toast.makeText(
                            requireContext(),
                            getString(R.string.success_update_kwh, kwhVal.name),
                            Toast.LENGTH_SHORT
                        ).show()
                        editKwhDialog.dismiss()
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        Toast.makeText(
                            requireContext(),
                            getString(R.string.failed_update_kwh, kwhVal.name),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
    }

    private fun deleteTenant(adapterId: Int) {
        val id = requireArguments().getInt("KEY_ID")
        val kwhVal = sharedModel.adapter[id].getKwhVal(adapterId)
        val builder =
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_KwhBankZ_Dialog).apply {
                setTitle(getString(R.string.delete_tenant))
                setMessage(getString(R.string.sure_delete, kwhVal.name))
                setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                setPositiveButton(getString(R.string.delete)) { dialog, _ ->
                    CoroutineScope(Dispatchers.IO).launch {
                        val numbDelete = db.deleteTenant(id, kwhVal.tenantId)
                        if (numbDelete > 0) {
                            withContext(Dispatchers.Main) {
                                sharedModel.adapter[id].removeItem(adapterId)
                                Toast.makeText(
                                    requireContext(),
                                    getString(R.string.deleting_tenant, kwhVal.name),
                                    Toast.LENGTH_SHORT
                                ).show()
                                dialog.dismiss()
                            }
                        }
                    }
                }
            }
        builder.create().show()
    }

    private fun updateTenantState(adapterId: Int) {
        val id = requireArguments().getInt("KEY_ID")
        val kwhVal = sharedModel.adapter[id].getKwhVal(adapterId)
        val builder =
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_KwhBankZ_Dialog).apply {
                setTitle(if (kwhVal.isActive) getString(R.string.disable_tenant) else getString(R.string.enable_tenant))
                setMessage(
                    if (kwhVal.isActive) getString(R.string.ask_disable, kwhVal.name) else
                        getString(
                            R.string.ask_enable, kwhVal.name
                        )
                )
                setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                setPositiveButton(if (kwhVal.isActive) getString(R.string.disable) else getString(R.string.enable)) { dialog, _ ->
                    CoroutineScope(Dispatchers.IO).launch {
                        val update = db.updateTenantState(id, kwhVal.tenantId, !kwhVal.isActive)
                        if (update > 0) {
                            withContext(Dispatchers.Main) {
                                sharedModel.adapter[id].toggleState(adapterId)
                                Toast.makeText(
                                    requireContext(),
                                    getString(
                                        R.string.state,
                                        kwhVal.name,
                                        kwhVal.isActive.toString()
                                    ),
                                    Toast.LENGTH_SHORT
                                ).show()
                                dialog.dismiss()
                            }
                        }
                    }
                }
            }
        builder.create().show()
    }

    private fun clearKwhValue(adapterId: Int) {
        val id = requireArguments().getInt("KEY_ID")
        val kwhVal = sharedModel.adapter[id].getKwhVal(adapterId)
        val builder =
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_KwhBankZ_Dialog).apply {
                setTitle(getString(R.string.clear_kwh_title))
                setMessage(
                    getString(R.string.clear_kwh_value_ask, kwhVal.name)
                )
                setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                setPositiveButton(getString(R.string.clear)) { dialog, _ ->
                    CoroutineScope(Dispatchers.IO).launch {
                        val clear = db.deleteKwh(id, kwhVal.kwhValueId!!)
                        if (clear > 0) {
                            withContext(Dispatchers.Main) {
                                sharedModel.adapter[id].clearKwhVal(adapterId)
                                Toast.makeText(
                                    requireContext(),
                                    getString(
                                        R.string.clear_success,
                                        kwhVal.name
                                    ),
                                    Toast.LENGTH_SHORT
                                ).show()
                                dialog.dismiss()
                            }
                        }
                    }
                }
            }
        builder.create().show()
    }

    private fun updateTenantName(adapterId: Int) {
        val id = requireArguments().getInt("KEY_ID")
        val kwhVal = sharedModel.adapter[id].getKwhVal(adapterId)
        val editTenantDialog = EditTenantDialog.newInstance(id, adapterId)
        editTenantDialog.show(parentFragmentManager, EditTenantDialog.TAG)
        editTenantDialog.setOnOkClicked {
            CoroutineScope(Dispatchers.IO).launch {
                val update = db.updateTenantName(id, kwhVal.tenantId, it)
                if (update > 0) {
                    withContext(Dispatchers.Main) {
                        sharedModel.adapter[id].changeTenantName(adapterId, it)
                        Toast.makeText(
                            requireContext(),
                            getString(R.string.tenant_name_changed, it),
                            Toast.LENGTH_SHORT
                        ).show()
                        editTenantDialog.dismiss()
                    }
                }
            }
        }
    }
}