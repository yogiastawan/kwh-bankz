package com.ether.kwhbankz.edit.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.ether.kwhbankz.R
import com.ether.kwhbankz.viewmodel.DataShared
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class InputKwhDialog : DialogFragment() {

    private var onOkClicked: ((String) -> Unit)? = null
    private lateinit var modelShared: DataShared

    companion object {
        const val TAG = "INPUT_KWH_DIALOG_TAG"
        fun newInstance(pos: Int, indexAdapter: Int): InputKwhDialog {
            val args = Bundle()
            args.putInt("POS", pos)
            args.putInt("ADAPTER_ID", indexAdapter)
            val fragment = InputKwhDialog()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        modelShared = ViewModelProvider(requireActivity()).get(DataShared::class.java)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val v = View.inflate(requireContext(), R.layout.input_kwh_dialog, null)

        val pos = requireArguments().getInt("POS")
        val idAdapter = requireArguments().getInt("ADAPTER_ID")

        val tvName: AppCompatTextView = v.findViewById(R.id.name)
        tvName.text = modelShared.adapter[pos].getKwhVal(idAdapter).name
        val tiLayout: TextInputLayout = v.findViewById(R.id.text_input_layout)
        val tiEditText: TextInputEditText = v.findViewById(R.id.edit_value_kwh)

        val builder =
            MaterialAlertDialogBuilder(requireContext(), R.style.Theme_KwhBankZ_Dialog).apply {
                setTitle(getString(R.string.input_kwh_value))
                setView(v)
                setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                setPositiveButton(getString(R.string.insert)) { _, _ ->
                    onOkClicked?.invoke(tiEditText.text.toString())
                }
            }
        val dialog = builder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
            if (tiEditText.text!!.isEmpty()) {
                tiLayout.error = getString(R.string.kwh_is_empty)
            }
        }
        tiEditText.requestFocus()
        lifecycleScope.launch {
            delay(200)
            val imm =
                tiEditText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(tiEditText, InputMethodManager.SHOW_IMPLICIT)
        }
        tiEditText.doOnTextChanged { text, _, _, _ ->
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = text!!.isNotBlank()
            if (text.isBlank()) {
                tiLayout.error = getString(R.string.kwh_is_blank)
            } else {
                tiLayout.error = null
            }
        }
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    fun setOnOKClicked(l: (String) -> Unit) {
        onOkClicked = l
    }
}