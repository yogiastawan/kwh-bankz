package com.ether.kwhbankz.data

data class DataKwh(var nameTenant: String? = null, var kwh: Double? = null)
