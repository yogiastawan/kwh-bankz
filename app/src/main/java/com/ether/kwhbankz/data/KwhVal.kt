package com.ether.kwhbankz.data

data class KwhVal(
    var tenantId: Int,
    var name: String,
    var kwhValueId: Int? = null,
    var kwh: Double? = null,
    var isActive: Boolean
)
