package com.ether.kwhbankz.about

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_COMPACT
import com.ether.kwhbankz.R
import com.ether.kwhbankz.about.dialog.UpdateApp
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.InetAddress
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class AboutApp : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.about)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_24)
        val tvVersion: AppCompatTextView = findViewById(R.id.tv_version)
        val pm = packageManager.getPackageInfo(packageName, 0)
        tvVersion.text = getString(R.string.v, pm.versionName)
        val tvSourceCode: AppCompatTextView = findViewById(R.id.tv_sourcecode_content)
        tvSourceCode.movementMethod = LinkMovementMethod.getInstance()
        val tvImportantContent: AppCompatTextView = findViewById(R.id.tv_important_content)
        val tvPurposeContent: AppCompatTextView = findViewById(R.id.tv_purpose_content)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvImportantContent.text =
                Html.fromHtml(getString(R.string.important_statement), FROM_HTML_MODE_COMPACT)
            tvPurposeContent.text =
                Html.fromHtml(getString(R.string.purpose_statement), FROM_HTML_MODE_COMPACT)
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            tvImportantContent.text =
                Html.fromHtml(getString(R.string.important_statement), null, TextViewTagHandler())
            tvPurposeContent.text =
                Html.fromHtml(getString(R.string.purpose_statement), null, TextViewTagHandler())
        }
        val fabUpdate: ExtendedFloatingActionButton = findViewById(R.id.fab_update)
        fabUpdate.setOnClickListener {
            checkUpdate()

        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun checkUpdate() {
        val scope = CoroutineScope(Job() + Dispatchers.IO)
        val dialog = UpdateApp.newInstance()
        dialog.show(supportFragmentManager, UpdateApp.TAG)
        dialog.setOnCancelClicked {
            scope.cancel()
        }
        scope.launch {
            if (!isDeviceConnected()) {
                Log.d("UPDATE", "No internet")
                withContext(Dispatchers.Main) {
                    dialog.dismiss()

                    val snackBar = Snackbar.make(
                        findViewById(R.id.about_root),
                        getString(R.string.no_internet),
                        Snackbar.LENGTH_INDEFINITE
                    )
                    snackBar.setAction("Ok") {
                        snackBar.dismiss()
                    }
                    snackBar.animationMode = Snackbar.ANIMATION_MODE_SLIDE
                    snackBar.show()
                }
                scope.cancel()
            }
            if (!isConnectedToInternet()) {
                Log.d("UPDATE", "No access server")
                withContext(Dispatchers.Main) {
                    dialog.dismiss()
                    val snackBar = Snackbar.make(
                        findViewById(R.id.about_root),
                        getString(R.string.cannot_access_server),
                        Snackbar.LENGTH_INDEFINITE
                    )
                    snackBar.setAction("Ok") {
                        snackBar.dismiss()
                    }
                    snackBar.animationMode = Snackbar.ANIMATION_MODE_SLIDE
                    snackBar.show()
                }
                scope.cancel()
            }

            try {
                val url =
                    URL("https://gitlab.com/yogiastawan/kwh-bankz-update-repo/-/raw/main/repository.repo")
                val connect = async { url.openConnection() as HttpsURLConnection }
                val a = connect.await()
                a.connect()
                if (a.responseCode == 200) {
                    val bufferedReader = BufferedReader(InputStreamReader(a.inputStream))
                    withContext(Dispatchers.Default) {
                        var line = bufferedReader.readLine()
                        val lines = mutableListOf<String>()
                        while (line != null) {
                            lines.add(line)
                            line = bufferedReader.readLine()
                        }
                        withContext(Dispatchers.Main) {
                            val currVersion = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                packageManager.getPackageInfo(packageName, 0).longVersionCode
                            } else {
                                packageManager.getPackageInfo(packageName, 0).versionCode

                            }
                            if (currVersion.toInt() < lines[0].toInt()) {
                                dialog.dismiss()
                                val snackBar = Snackbar.make(
                                    findViewById(R.id.about_root),
                                    getString(R.string.new_version_available),
                                    Snackbar.LENGTH_LONG
                                )
                                snackBar.setAction("Download") {
                                    snackBar.dismiss()
                                    val intentOpen = Intent(Intent.ACTION_VIEW, Uri.parse(lines[1]))
                                    startActivity(intentOpen)
                                }
                                snackBar.animationMode = Snackbar.ANIMATION_MODE_SLIDE
                                snackBar.show()
                            } else {
                                dialog.dismiss()
                                val snackBar = Snackbar.make(
                                    findViewById(R.id.about_root), getString(R.string.newest_app),
                                    Snackbar.LENGTH_INDEFINITE
                                )
                                snackBar.setAction("Ok") {
                                    snackBar.dismiss()
                                }
                                snackBar.animationMode = Snackbar.ANIMATION_MODE_SLIDE
                                snackBar.show()
                            }
                        }
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        dialog.dismiss()
                        val snackBar = Snackbar.make(
                            findViewById(R.id.about_root), getString(R.string.cannot_download_repo),
                            Snackbar.LENGTH_INDEFINITE
                        )
                        snackBar.setAction("Ok") {
                            snackBar.dismiss()
                        }
                        snackBar.animationMode = Snackbar.ANIMATION_MODE_SLIDE
                        snackBar.show()
                    }
                }
                a.disconnect()
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    dialog.dismiss()
                    val snackBar = Snackbar.make(
                        findViewById(R.id.about_root), getString(R.string.error, e.message),
                        Snackbar.LENGTH_INDEFINITE
                    )
                    snackBar.setAction("Ok") {
                        snackBar.dismiss()
                    }
                    snackBar.animationMode = Snackbar.ANIMATION_MODE_SLIDE
                    snackBar.show()
                }
            }
        }
    }

    private fun isDeviceConnected(): Boolean {
        val cm =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val act = cm.activeNetwork ?: return false
            val networkCap = cm.getNetworkCapabilities(act) ?: return false
            return when {
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_VPN) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_USB) -> true
//                networkCap.hasTransport(NetworkCapabilities.NET_CAPABILITY_INTERNET) -> true
                else -> false

            }
        } else {
            val netInfo = cm.activeNetworkInfo ?: return false
            return netInfo.isConnected
        }
    }

    private fun isConnectedToInternet(): Boolean {
        return try {
            val ip = InetAddress.getByName("gitlab.com")
            !ip.equals("")
        } catch (e: Exception) {
            false
        }
    }
}