package com.ether.kwhbankz.about

import android.text.Editable
import android.text.Html
import android.text.Spannable
import android.text.Spanned
import android.text.style.StrikethroughSpan
import org.xml.sax.XMLReader


class TextViewTagHandler : Html.TagHandler {
    override fun handleTag(
        opening: Boolean,
        tag: String?,
        output: Editable?,
        xmlReader: XMLReader?
    ) {
        if (tag == "li" && !opening) {
            output?.append(" ")
        }
        if (tag == "s" && opening) {
            output?.setSpan(
                StrikethroughSpan(),
                output.length,
                output.length,
                Spanned.SPAN_MARK_MARK
            )
        } else if (tag == "s" && !opening) {

            val obj = getLast(output!!, StrikethroughSpan::class.java)
            val start = output.getSpanStart(obj)
            output.removeSpan(obj)
            output.setSpan(
                StrikethroughSpan(),
                start,
                output.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }

    private fun getLast(text: Editable, kind: Class<*>): Any? {
        val obj = text.getSpans(0, text.length, kind)
        return if (obj.isEmpty()) {
            null
        } else {
            for (i in obj.size downTo 1) {
                if (text.getSpanFlags(obj[i - 1]) == Spannable.SPAN_MARK_MARK) {
                    return obj[i - 1]
                }
            }
            null
        }
    }
}