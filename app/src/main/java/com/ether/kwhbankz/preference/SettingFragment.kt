package com.ether.kwhbankz.preference

import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.ether.kwhbankz.R

class SettingFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preference, rootKey)
        val themePref: ListPreference? = findPreference(getString(R.string.pref_key_theme))

        themePref?.onPreferenceChangeListener = themeChangeListener
    }

    private val themeChangeListener =
        Preference.OnPreferenceChangeListener { _, newValue ->
            when (newValue) {
                getString(R.string.pref_dark_mode) -> {
                    updateTheme(AppCompatDelegate.MODE_NIGHT_YES)
                    true
                }
                getString(R.string.pref_light_mode) -> {
                    updateTheme(AppCompatDelegate.MODE_NIGHT_NO)
                    true
                }
                getString(R.string.pref_system_mode) -> {
                    updateTheme(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                    true
                }
                else -> false
            }
        }


    private fun updateTheme(mode: Int) {
        AppCompatDelegate.setDefaultNightMode(mode)
        requireActivity().recreate()
    }
}
