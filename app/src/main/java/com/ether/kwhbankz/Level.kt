package com.ether.kwhbankz

val TB_NAME: List<String> =
    listOf("GF", "F1", "TRANS_STUDIO", "TRANS_MART", "STORE", "SERVER", "PUBLIC")
const val TB_DATE: String = "DATE"
const val TB_LABEL = "LABEL"
const val TB_KWH = "KWH"
const val TB_TENANT = "TENANT"

val LEVEL_LINK = arrayOf(
    "https://gitlab.com/yogiastawan/csv-tenant/-/raw/main/gf.csv",
    "https://gitlab.com/yogiastawan/csv-tenant/-/raw/main/f1.csv",
    "https://gitlab.com/yogiastawan/csv-tenant/-/raw/main/trans_studio.csv",
    "https://gitlab.com/yogiastawan/csv-tenant/-/raw/main/transmart.csv",
    "https://gitlab.com/yogiastawan/csv-tenant/-/raw/main/store.csv",
    "https://gitlab.com/yogiastawan/csv-tenant/-/raw/main/server.csv",
    "https://gitlab.com/yogiastawan/csv-tenant/-/raw/main/tenant_public.csv"
)

val LOADING_STATE = arrayOf(
    "Checking network connection",
    "Checking server",
    "Starting Service",
    "Downloading data for GF",
    "Extract data GF",
    "Inserting data GF to database",
    "Downloading data for F1",
    "Extract data F1",
    "Inserting data F1 to database",
    "Downloading data for Trans Studio",
    "Extract data Trans Studio",
    "Inserting data Trans Studio to database",
    "Downloading data for Transmart",
    "Extract data Transmart",
    "Inserting data Transmart to database",
    "Downloading data for Store",
    "Extract data Store",
    "Inserting data Store to database",
    "Downloading data for Server",
    "Extract data Server",
    "Inserting data Server to database",
    "Downloading data for Public",
    "Extract data Public",
    "Inserting data Public to database",
    "Initialize data done"
)