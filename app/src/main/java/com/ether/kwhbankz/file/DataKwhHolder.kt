package com.ether.kwhbankz.file

import com.ether.kwhbankz.data.DataKwh

data class DataKwhHolder(
    val data: MutableList<DataKwh> = MutableList(7) {
        DataKwh()
    }
)
