package com.ether.kwhbankz.file

import android.content.Context
import android.net.Uri
import com.ether.kwhbankz.R
import com.ether.kwhbankz.data.DataKwh
import com.ether.kwhbankz.db.DataBaseHelper
import com.ether.kwhbankz.db.model.TenantRecord
import com.ether.kwhbankz.edit.dialog.SaveFileLoadingDialog
import kotlinx.coroutines.*
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.InputStreamReader
import java.io.OutputStreamWriter

class CsvFile(private val context: Context) {

    companion object {
        fun getTenantList(reader: BufferedReader): List<TenantRecord> {
            val list = mutableListOf<TenantRecord>()
            var line: String? = reader.readLine()
            while (line != null) {
                val idx = line.indexOf(',')
                if (idx == 0) {
                    line = reader.readLine()
                    continue
                }
                val name = line.substring(0, idx)
                val state = line.substring(idx + 1)
                list.add(TenantRecord(-1, name, state == "1"))
                line = reader.readLine()
            }
            return list
        }
    }

    fun getTenantList(fileUri: Uri): List<TenantRecord> {
        val fileInputStream = context.contentResolver.openInputStream(fileUri)
        val list = mutableListOf<TenantRecord>()
        val reader = BufferedReader(InputStreamReader(fileInputStream))
        var line: String? = reader.readLine()
        while (line != null) {
            val idx = line.indexOf(',')
            if (idx == 0) {
                line = reader.readLine()
                continue
            }
            val name = line.substring(0, idx)
            val state = line.substring(idx + 1)
            list.add(TenantRecord(-1, name, state == "1"))
            line = reader.readLine()
        }
        reader.close()
        return list
    }

    suspend fun writeFileCSV(fileUri: Uri, dateId: Int, date:String, dialog: SaveFileLoadingDialog) {
        withContext(Dispatchers.IO) {
            yield()
            val l = context.resources.getStringArray(R.array.level_group).size
            val db = DataBaseHelper(context, l)
            val adapt = mutableListOf<List<DataKwh>>()
            for (i: Int in 0 until l) {
                yield()
                val a = async { db.getDataList(i, dateId) }
                a.await().let { adapt.add(it) }
            }
            val dataParser = mutableListOf<DataKwhHolder>()
            withContext(Dispatchers.Default) {
                var c = 0
                while (c < adapt[0].size || c < adapt[1].size || c < adapt[2].size || c < adapt[3].size || c < adapt[4].size || c < adapt[5].size || c < adapt[6].size) {
                    yield()
                    val dataKwhHolder = DataKwhHolder()
                    for (i: Int in 0 until l) {
                        yield()
                        if (c < adapt[i].size) {
                            dataKwhHolder.data[i] = adapt[i][c]
                        }
                    }
                    dataParser.add(dataKwhHolder)
                    c++
                }
            }
            val streamDef = async { context.contentResolver.openOutputStream(fileUri) }
            val bufferedWriter = BufferedWriter(OutputStreamWriter(streamDef.await()))
            val arr = context.resources.getStringArray(R.array.level_group)
            bufferedWriter.appendLine("$date,,,,,,,,,,,,,,,,,,,,,")
            bufferedWriter.appendLine(arr.joinToString(",,,"))
            bufferedWriter.appendLine(",,,,,,,,,,,,,,,,,,,,,")
            for (i: Int in 0 until dataParser.size) {
                yield()
                var msg = ""
                for (j: Int in 0 until 7) {
                    yield()
                    val kwhVal = dataParser[i].data[j]
                    msg += "${kwhVal.nameTenant ?: ""},"
                    msg += if (kwhVal.kwh != null) {
                        if (kwhVal.kwh!! > 0) {
                            "${kwhVal.kwh},,"
                        } else {
                            ",,"
                        }
                    } else {
                        ",,"
                    }

                }
                bufferedWriter.appendLine(msg)
            }
            bufferedWriter.close()
            withContext(Dispatchers.Main) {
                dialog.setOnCancelClicked {
                    this.cancel()
                }
                delay(500)
                dialog.dismiss()
            }
        }
    }

}