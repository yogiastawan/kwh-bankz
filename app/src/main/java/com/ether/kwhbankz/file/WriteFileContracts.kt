package com.ether.kwhbankz.file

import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContracts

class WriteFileContracts(private val type: String) : ActivityResultContracts.CreateDocument() {
    override fun createIntent(context: Context, input: String): Intent {
        return super.createIntent(context, input).setType(type)
    }
}