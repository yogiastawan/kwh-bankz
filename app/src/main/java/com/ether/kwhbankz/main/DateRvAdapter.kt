package com.ether.kwhbankz.main

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.ether.kwhbankz.R
import com.ether.kwhbankz.db.model.DateRecord

class DateRvAdapter : RecyclerView.Adapter<DateRvHolder>() {
    private var dateList = mutableListOf<DateRecord>()

    private var onItemClick: ((DateRecord) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DateRvHolder {
        val v: View =
            LayoutInflater.from(parent.context).inflate(R.layout.rv_date_item, parent, false)
        val holder = DateRvHolder(v) {
            onItemClick?.invoke(dateList[it])
        }
        return holder
    }

    override fun onBindViewHolder(holder: DateRvHolder, position: Int) {
        holder.binViewHolder(dateList[position])
    }

    override fun getItemCount(): Int {
        return dateList.size
    }

    fun setOnItemClick(date: ((DateRecord) -> Unit)?) {
        onItemClick = date
    }

    fun addItem(date: DateRecord) {
        dateList.add(date)
        notifyItemInserted(itemCount - 1)
    }

    fun clear() {
        val size = itemCount
        dateList.clear()
        notifyItemRangeRemoved(0, size)
    }
}