package com.ether.kwhbankz.main.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.ether.kwhbankz.LEVEL_LINK
import com.ether.kwhbankz.MainActivity
import com.ether.kwhbankz.R
import com.ether.kwhbankz.db.DataBaseHelper
import com.ether.kwhbankz.file.CsvFile
import kotlinx.coroutines.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class InitService : Service() {

    private val scope: CoroutineScope = CoroutineScope(Job() + Dispatchers.IO)

    companion object {
        const val NOTIF_ID = 1
        const val BROADCASTER = "com.ether.kwhbankz.broadcaster"
        const val NOTIFICATION_CHANNEL_ID = "INIT_SERVICE"
        const val PROGRESS = "$BROADCASTER.progress"
        const val ERROR = "$BROADCASTER.error"
        const val SUCCESS = "$BROADCASTER.success"
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        val notification = createNotification()
        startForeground(NOTIF_ID, notification)
        scope.launch {
            var i = 3
            var length: Int
            withContext(Dispatchers.Main) {
                length = applicationContext.resources.getStringArray(R.array.level_group).size
            }
            val db = DataBaseHelper(applicationContext, length)
            for (link in LEVEL_LINK) {
                yield()
                try {
                    //get url
                    broadcastProgress(i)
                    val url = URL(link)

                    broadcastProgress(i++)
                    val urlConnection = url.openConnection() as HttpsURLConnection
                    urlConnection.connect()
                    if (urlConnection.responseCode == 200) {
                        broadcastProgress(i++)
                        val bufferedReader =
                            BufferedReader(InputStreamReader(urlConnection.inputStream))
                        withContext(Dispatchers.Default) {
                            val list = CsvFile.getTenantList(bufferedReader)
                            for (tenant in list) {
                                val a = db.insertTenant(((i - 3) / 3), tenant)
                                if (a <= 0) {
                                    broadcastError("Cannot insert tenant to database")

                                }
                            }
                        }
                        i++
                    } else {
                        broadcastError("Error: ${urlConnection.responseMessage}")
                        this.cancel()
                        stopSelf()
                        break
                    }
                    urlConnection.disconnect()
                } catch (e: Exception) {
                    broadcastError("Error: ${e.message}")
                    stopSelf()
                    break
                }

            }
            if (this.isActive) {
                broadcastSuccess()
            }
        }
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        scope.cancel()
        stopForeground(true)
        super.onDestroy()
    }

    private fun broadcastProgress(value: Int) {
        val intent = Intent(BROADCASTER)
        intent.putExtra(PROGRESS, value)
        sendBroadcast(intent)
    }

    private fun broadcastError(msgError: String) {
        val intent = Intent(BROADCASTER)
        intent.putExtra(ERROR, msgError)
        sendBroadcast(intent)
    }

    private fun broadcastSuccess() {
        stopSelf()
        val intent = Intent(BROADCASTER)
        intent.putExtra(SUCCESS, true)
        sendBroadcast(intent)
    }

    private fun createNotification(): Notification {
        val intentMain = Intent(applicationContext, MainActivity::class.java)
        intentMain.putExtra("IS_SHOW_DIALOG", true)
        intentMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        val pendingIntent = PendingIntent.getActivity(
            applicationContext,
            0,
            intentMain,
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE else PendingIntent.FLAG_UPDATE_CURRENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                "Initializing Database",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationChannel.description = "Initializing database on progress"
            notificationChannel.setSound(null, null)
            val notificationManager =
                applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }
        val notification = NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL_ID)
        notification.setContentTitle("Initializing Database")
        notification.setContentText("Please wait...")
        notification.setContentIntent(pendingIntent)
        notification.setWhen(System.currentTimeMillis())
        notification.setSmallIcon(R.drawable.ic_launcher_foreground)
        return notification.build()
    }
}