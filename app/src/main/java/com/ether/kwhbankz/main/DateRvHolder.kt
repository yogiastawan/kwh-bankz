package com.ether.kwhbankz.main

import android.os.Build
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.os.ConfigurationCompat
import androidx.recyclerview.widget.RecyclerView
import com.ether.kwhbankz.R
import com.ether.kwhbankz.db.model.DateRecord
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.material.card.MaterialCardView
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle

class DateRvHolder(itemView: View, onItemClick: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {

    private val dateTv: AppCompatTextView = itemView.findViewById(R.id.tv_date)
    private val subDateTv: AppCompatTextView = itemView.findViewById(R.id.tv_sub_date)
    private val dayTv: AppCompatTextView = itemView.findViewById(R.id.tv_day)
    private val cardView: MaterialCardView = itemView.findViewById(R.id.date_card_view)

    init {
        itemView.setOnClickListener {
            onItemClick(adapterPosition)
        }
    }

    fun binViewHolder(date: DateRecord) {
        val lp = cardView.layoutParams
        if (lp is FlexboxLayoutManager.LayoutParams) {
            lp.flexGrow = 1f
        }
        val locale = ConfigurationCompat.getLocales(itemView.resources.configuration).get(0)
        var dayDate = ""
        var subDate = ""
        var day = ""
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val dateParse = DateTimeFormatter.ofPattern("dd-MM-yyyy")
            val dateTime = LocalDate.parse(date.date, dateParse)
            dayDate = dateTime.dayOfMonth.toString()
            subDate = "${dateTime.month.getDisplayName(TextStyle.FULL, locale)} ${dateTime.year}"
            day = dateTime.dayOfWeek.getDisplayName(TextStyle.FULL, locale)
        } else {
            val formatter = SimpleDateFormat("dd-MM-yyyy", locale)
            val dateTime = formatter.parse(date.date)
            dateTime?.let {
                dayDate = SimpleDateFormat("dd", locale).format(it)
                subDate =
                    "${SimpleDateFormat("MMMM", locale).format(it)} ${
                        SimpleDateFormat(
                            "yyyy",
                            locale
                        ).format(it)
                    }"
                day = SimpleDateFormat("EEEE", locale).format(it)
            }

        }
        dateTv.text = dayDate
        subDateTv.text = subDate
        dayTv.text = day

        //set stroke color
        if (date.isWeekly) {
            cardView.strokeColor = ContextCompat.getColor(itemView.context, R.color.weekly_color)
        } else {
            cardView.strokeColor = ContextCompat.getColor(itemView.context, R.color.default_color)
        }

        if (date.isMonthly) {
            cardView.strokeColor =
                ContextCompat.getColor(itemView.context, R.color.monthly_color)
        }
    }

}