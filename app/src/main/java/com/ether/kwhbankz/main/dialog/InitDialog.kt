package com.ether.kwhbankz.main.dialog

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.DialogFragment
import com.ether.kwhbankz.LOADING_STATE
import com.ether.kwhbankz.R
import com.ether.kwhbankz.main.service.InitService
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.progressindicator.CircularProgressIndicator

class InitDialog : DialogFragment() {

    private var onCanceled: (() -> Unit)? = null
    private var onSuccesses: (() -> Unit)? = null

    private lateinit var progressIndicator: CircularProgressIndicator
    private lateinit var tvInit: AppCompatTextView

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, intent: Intent?) {
            val bundle = intent!!.extras
            if (bundle != null) {
                val progress = bundle.getInt(InitService.PROGRESS, -1)
                if (progress >= 0) {
                    progressIndicator.progress = progress * 100 / (LOADING_STATE.size - 1)
                    tvInit.text = LOADING_STATE[progress]
                }
                val error = bundle.getString(InitService.ERROR, "")
                if (error.isNotEmpty()) {
                    tvInit.text = error
                }
                val success = bundle.getBoolean(InitService.SUCCESS, false)
                if (success) {
                    dialog!!.dismiss()
                    onSuccesses?.invoke()
                }
            }
        }

    }


    companion object {
        const val TAG = "INIT_DIALOG_TAG"
        fun newInstance(): InitDialog {

            return InitDialog()
        }
    }

    override fun onResume() {
        super.onResume()
        requireContext().registerReceiver(broadcastReceiver, IntentFilter(InitService.BROADCASTER))
    }

    override fun onPause() {
        super.onPause()
        requireContext().unregisterReceiver(broadcastReceiver)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = View.inflate(requireContext(), R.layout.init_dialog, null)
        tvInit = view.findViewById(R.id.tv_init)
        progressIndicator = view.findViewById(R.id.progress_init)
        val builder = MaterialAlertDialogBuilder(requireActivity()).apply {
            setTitle(getString(R.string.init_db))
            setView(view)
            setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                onCanceled?.invoke()
                dialog.dismiss()
            }
        }

        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    fun setOnCanceled(l: () -> Unit) {
        onCanceled = l
    }

    fun setOnSuccesses(l: () -> Unit) {
        onSuccesses = l
    }
}