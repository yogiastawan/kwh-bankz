package com.ether.kwhbankz

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatTextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.os.ConfigurationCompat
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.ether.kwhbankz.about.AboutApp
import com.ether.kwhbankz.db.DataBaseHelper
import com.ether.kwhbankz.db.model.DateRecord
import com.ether.kwhbankz.edit.EditActivity
import com.ether.kwhbankz.main.dialog.InitDialog
import com.ether.kwhbankz.main.service.InitService
import com.ether.kwhbankz.preference.Preference
import com.ether.kwhbankz.viewmodel.DataShared
import com.google.android.flexbox.*
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.*
import java.net.URL
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.net.ssl.HttpsURLConnection


class MainActivity : AppCompatActivity() {

    private lateinit var mainLayout: CoordinatorLayout

    private lateinit var rVDate: RecyclerView
    private lateinit var tvNoData: AppCompatTextView
    private lateinit var fabNewTask: FloatingActionButton

    private lateinit var level: Array<String>

    //database
    private lateinit var db: DataBaseHelper

    //view model
    private lateinit var sharedModel: DataShared

    //preference
    private lateinit var preference: SharedPreferences

    private val prefChange =
        SharedPreferences.OnSharedPreferenceChangeListener { _, _ ->
            Log.d("PrefMain", "Changed")
            sharedModel.dateRvAdapter.notifyItemRangeChanged(0, sharedModel.dateRvAdapter.itemCount)
        }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_bar_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val sharedReference = getSharedPreferences("KwhBankZSharedRef", MODE_PRIVATE)
        menu?.findItem(R.id.menu_item_init)?.isEnabled =
            !sharedReference.getBoolean("isInitialized", false)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_init -> {
                initData()
                true
            }
            R.id.menu_item_setting -> {
                startActivity(Intent(this, Preference::class.java))
                true
            }
            R.id.menu_item_about -> {
                val about = Intent(this, AboutApp::class.java)
                startActivity(about)
                true
            }
            else -> {
                false
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //view model
        sharedModel = ViewModelProvider(this).get(DataShared::class.java)

        //get preference
        preference = PreferenceManager.getDefaultSharedPreferences(this)
        CoroutineScope(Dispatchers.Main).launch {
            when (preference.getString(getString(R.string.pref_key_theme), null)) {
                getString(R.string.pref_dark_mode) -> {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                }
                getString(R.string.pref_light_mode) -> {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                }
                else -> {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                }
            }
        }
        level = resources.getStringArray(R.array.level_group)
        db = DataBaseHelper(applicationContext, level.size)
        mainLayout = findViewById(R.id.main_root)
        rVDate = findViewById(R.id.rvData)
        tvNoData = findViewById(R.id.tv_no_data_main)
        fabNewTask = findViewById(R.id.fab_new_task)
        fabNewTask.setOnClickListener {
            newTask()
        }
        val layoutManager = FlexboxLayoutManager(applicationContext).apply {
            flexDirection = FlexDirection.ROW
            flexWrap = FlexWrap.WRAP
            justifyContent = JustifyContent.FLEX_START

        }
        rVDate.layoutManager = layoutManager
        val divider = FlexboxItemDecoration(
            rVDate.context
        )
        divider.setDrawable(
            AppCompatResources.getDrawable(
                applicationContext,
                R.drawable.recyclerview_item_divider
            )!!
        )
        divider.setOrientation(FlexboxItemDecoration.BOTH)

        rVDate.addItemDecoration(divider)
        rVDate.adapter = sharedModel.dateRvAdapter
        sharedModel.dateRvAdapter.setOnItemClick {
            if (it.id > 0) {
                val intent = Intent(this, EditActivity::class.java)
                intent.putExtra("DATE_ID", it.id)
                intent.putExtra("DATE_STR", it.date)
                intent.putExtra("DATE_WEEKLY", it.isWeekly)
                intent.putExtra("DATE_MONTHLY", it.isMonthly)
                startActivity(intent)
                sharedModel.dateRvAdapter.clear()
            } else {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.cannot_open_id, it.id),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        val toolbar: MaterialToolbar = findViewById(R.id.toolbar_main_activity)
        setSupportActionBar(toolbar)

        if (intent.getBooleanExtra("IS_SHOW_DIALOG", false)) {
            showDialogInit()
        }
    }

    override fun onResume() {
        super.onResume()
        CoroutineScope(Dispatchers.IO).launch {
            preference.registerOnSharedPreferenceChangeListener(prefChange)
            if (sharedModel.dateRvAdapter.itemCount <= 0) {
                val numb = async { db.getAllDate(sharedModel.dateRvAdapter) }
                if (numb.await() <= 0) {
                    withContext(Dispatchers.Main) {
                        tvNoData.visibility = View.VISIBLE
                        rVDate.visibility = View.GONE
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        tvNoData.visibility = View.GONE
                        rVDate.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        preference.unregisterOnSharedPreferenceChangeListener(prefChange)
    }

    private fun broadcastProgress(value: Int) {
        val intent = Intent(InitService.BROADCASTER)
        intent.putExtra(InitService.PROGRESS, value)
        sendBroadcast(intent)
    }

    private fun broadcastError(msg: String) {
        val intent = Intent(InitService.BROADCASTER)
        intent.putExtra(InitService.ERROR, msg)
        sendBroadcast(intent)
    }

    private fun isDeviceConnected(): Boolean {
        val cm =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val act = cm.activeNetwork ?: return false
            val networkCap = cm.getNetworkCapabilities(act) ?: return false
            return when {
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_VPN) -> true
                networkCap.hasTransport(NetworkCapabilities.TRANSPORT_USB) -> true
//                networkCap.hasTransport(NetworkCapabilities.NET_CAPABILITY_INTERNET) -> true
                else -> false

            }
        } else {
            val netInfo = cm.activeNetworkInfo ?: return false
            return netInfo.isConnected
        }
    }

    private suspend fun isConnectedToInternet(): Boolean {
        var access: Boolean
        withContext(Dispatchers.IO) {
            val url = URL("https://gitlab.com")
            val connection = url.openConnection() as HttpsURLConnection
            connection.requestMethod = "HEAD"
            access = connection.responseCode == 200
        }
        return access
    }

    private fun initData() {
        showDialogInit()
        val scope = CoroutineScope(Job() + Dispatchers.Main)
        val intent = Intent(this, InitService::class.java)
        scope.launch {
            //check internet
            broadcastProgress(0)
            val isConnect = isDeviceConnected()

            if (!isConnect) {
                broadcastError(getString(R.string.no_internet))
                scope.cancel()
            }
            broadcastProgress(1)
            val isConnectInternet = isConnectedToInternet()

            if (!isConnectInternet) {
                broadcastError(getString(R.string.cannot_access_server))
                scope.cancel()
            }
            if (scope.isActive) {
                broadcastProgress(2)
                ContextCompat.startForegroundService(applicationContext, intent)
            }
        }
    }

    private fun showDialogInit() {
        val initDialog: InitDialog = InitDialog.newInstance()
        val sharedReference = getSharedPreferences("KwhBankZSharedRef", MODE_PRIVATE)
        initDialog.show(supportFragmentManager, InitDialog.TAG)
        initDialog.setOnCanceled {
            val editor = sharedReference.edit()
            editor.putBoolean("isInitialized", false)
            editor.apply()
            val intent = Intent(this, InitService::class.java)
            stopService(intent)
            //deleting all table
            CoroutineScope(Dispatchers.IO).launch {
                withContext(Dispatchers.Main) {
                    broadcastError(getString(R.string.cancel_init))
                }
                db.deleteTables()
                withContext(Dispatchers.Main) {
                    initDialog.dismiss()
                }
            }
        }
        initDialog.setOnSuccesses {
            val editor = sharedReference.edit()
            editor.putBoolean("isInitialized", true)
            editor.apply()
            invalidateOptionsMenu()
            val snackBar = Snackbar.make(
                mainLayout,
                getString(R.string.init_done),
                Snackbar.LENGTH_INDEFINITE
            )
            snackBar.setAction("Ok") {
                snackBar.dismiss()
            }
            snackBar.animationMode = Snackbar.ANIMATION_MODE_SLIDE
            snackBar.show()
        }
    }

    private fun newTask() {
        val weekly =
            preference.getString(getString(R.string.pref_key_weekly_date), null)?.toInt() ?: 0
        val monthly =
            preference.getString(getString(R.string.pref_key_monthly_date), null)?.toInt() ?: 0
        var isWeekly = false
        var isMonthly = false
        val date = DateRecord()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val dateParse = DateTimeFormatter.ofPattern("dd-MM-yyyy")
            val dateTime = LocalDate.parse(date.date, dateParse)
            val dayDate = dateTime.dayOfMonth
            isWeekly = (dayDate - weekly) % 7 == 0
            isMonthly = dayDate == monthly
        } else {
            val locale = ConfigurationCompat.getLocales(resources.configuration).get(0)
            val formatter = SimpleDateFormat("dd-MM-yyyy", locale)
            val dateTime = formatter.parse(date.date)
            dateTime?.let {
                val dayDate = SimpleDateFormat("dd", locale).format(it)
                isWeekly = (dayDate.toInt() - weekly) % 7 == 0
                isMonthly = dayDate.toInt() == monthly
            }
        }
        date.isWeekly = isWeekly
        date.isMonthly = isMonthly
        val idDate = db.insertDate(date)
        if (idDate > 0) {
            date.id = idDate.toInt()
            sharedModel.dateRvAdapter.addItem(date)
            val intent = Intent(this, EditActivity::class.java)
            intent.putExtra("DATE_ID", date.id)
            intent.putExtra("DATE_STR", date.date)
            intent.putExtra("DATE_WEEKLY", isWeekly)
            intent.putExtra("DATE_MONTHLY", isMonthly)
            startActivity(intent)
            sharedModel.dateRvAdapter.clear()
        } else {
            Toast.makeText(
                applicationContext,
                getString(R.string.task, date.date),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

}
